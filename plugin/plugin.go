package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks if the file is a Gemfile or if the extension is .rb
func Match(path string, info os.FileInfo) (bool, error) {
	if info.Name() == "Gemfile" {
		return true, nil
	} else if filepath.Ext(info.Name()) == ".rb" {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("brakeman", Match)
}
